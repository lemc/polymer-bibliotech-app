/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { html, PolymerElement } from "@polymer/polymer/polymer-element.js";
import '@polymer/iron-form/iron-form.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-button/paper-button.js';
import "./shared-styles.js";

class SignUp extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
      </style>

      <div class="card">
        <h1>Sign Up</h1>
        <iron-form>
          <form action="/users/signup">
            <paper-input label="email">
              <iron-icon icon="mail" slot="prefix"></iron-icon>
            </paper-input>
            <paper-input label="password" type="password"></paper-input>
            <div style="display: flex; justify-content: flex-end;">
              <paper-button type="submit">Sign Up</paper-button>
            </div>
          </form>
        </iron-form>
      </div>
    `;
  }
}

window.customElements.define("sign-up", SignUp);
