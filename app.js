const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const app = express();

const User = require('./db/User');
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

const saveUserToDb = user => {
  return user.save(err => {
    if (err) {
      console.log(`oops didn't manage to save our ${user.email} : (`, err);
      return;
    }
    console.log('user got saved !!', user);
  });
};

const findUserInDb = user => {
  const { email } = user;
  return User.find({ email }, (err, users) => {
    if (err) throw err;

    console.log(users);
  });
};

mongoose.Promise = global.Promise;

mongoose
  .connect('mongodb://localhost/node-auth')
  .then(() => console.log('db connection successful'))
  .catch(err => console.error(err));

app.get('/', (req, res) => res.send('Hello world!'));

app.post('/users/signin', (req, res) => {
  console.log(req, res);
  const { user } = req;

  findUserInDb(user).then(console.log);
});

app.post('/users/signup', (req, res) => {
  console.log(req, res);
  const { email, password } = req;
  const newUser = new User({
    name: email,
    email: email,
    password: password
  });

  saveUserToDb(newUser);
});

app.listen(3000, () => console.log('Example app listening on 3000!'));

const root = '/build/es5-bundled';
const options = {
  dotfiles: 'ignore',
  etag: false,
  index: false,
  maxAge: '1d',
  redirect: false,
  setHeaders: (res, path, stat) => {
    res.set('x-timestamp', Date.now());
  }
};

app.use(
  require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(root, [options]));
